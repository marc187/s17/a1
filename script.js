let students = [`Allen`, `John`, `Jennie`, `Tj`];

// Add Student
function addStudent(studentName){
    students.push(studentName);
    console.log(`${studentName} was added to the student's list.`);
}

// Count Student
function countStudents(){
    console.log(`There are a total of ${students.length} students enrolled.`)
}

//Sort and Print All Student
function printStudent(){
    students.sort((a, b) => {
        return a - b;
    });
    students.forEach(student => {
        console.log(student);
    });
}

//Find a student
function findStudent(student){
    if(student.length === 1){
        let filteredStudents = (arr, query) => {
            return arr.filter(el => el.toLowerCase().indexOf(query.toLowerCase()) === 0)
        }
        if(filteredStudents(students, student).length === 1){
            console.log(`${filteredStudents(students, student)} is an enrollee.`);
        } else if(filteredStudents(students, student).length > 1){
            console.log(`${filteredStudents(students, student)} are enrollees.`);
        } else {
            console.log(`${student} doesn't match anything.`);
        }
    } else{
        if (students.includes(student)){
            console.log(`${student} is an Enrollee.`)
        }else {
            console.log(`No student found with the name ${student}`)
        }
    }
}

//Add Section
function addSection(section){
    let studentWithSection = students.map(student => {
        student += ` - Section ${section}`;
        return student;
    });
    console.log(studentWithSection);
}

//Remove a student
function removeStudent(student){
    if(students.includes(student)){
        let studentToRemove = students.indexOf(student);
        students.splice(studentToRemove, 1);
        console.log(`${student} was removed from the student's list.`)
    } else {
        console.log(`This student is not an enrolee.`);
    }
}





